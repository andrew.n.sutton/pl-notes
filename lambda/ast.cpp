
#include "ast.hpp"

#include <cassert>
#include <iostream>

bool
is_value(const expr* e)
{
  switch (e->kind) {
    default:
      return false;
    case ek_abs:
    case ek_bool:
    case ek_ph:
    case ek_lambda:
      return true;
  }
}

bool
is_lambda(const expr* e)
{
  return e->kind == ek_abs || e->kind == ek_lambda;
}

// -------------------------------------------------------------------------- //
// Declarations

var*
scope::declare(const std::string& s) 
{
  assert(count(s) == 0);
  var* v = new var(s);
  emplace(s, v);  
  return v;
}

var*
scope::lookup(const std::string& s)
{
  auto iter = find(s);
  if (iter != end())
    return iter->second;
  else
    return nullptr;
}

var*
environment::declare(const std::string& s) 
{
  return stack.back().declare(s);
}

var*
environment::lookup(const std::string& s)
{
  for (auto iter = stack.rbegin(); iter != stack.rend(); ++iter) {
    if (var* v = iter->lookup(s))
      return v;
  } 
  return nullptr;
}

