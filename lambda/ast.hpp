#pragma once

#include <string>
#include <unordered_map>
#include <vector>

enum expr_kind : int {
  // Pure lambda calculus
  ek_ref,
  ek_abs,
  ek_app,

  // Basic boolean expressions
  ek_bool,
  ek_cond,

  // Syntactic sugar
  ek_ph,
  ek_lambda,
  ek_call,
  ek_bind,
  ek_let,
  ek_seq,
};

/// A variable in an abstraction.
struct var
{
  /// Constructs an unbound variable.
  var(const std::string& n)
    : name(n)
  { }
  
  std::string name; // The name of the variable.
};

/// A sequence of variables.
using var_seq = std::vector<var*>;

/// The base class of all expressions.
struct expr
{
  expr(expr_kind k)
    : kind(k)
  { }

  expr_kind kind;
};

/// A sequence of expressions.
using expr_seq = std::vector<expr*>;

/// A reference to a parameter.
struct ref_expr : expr
{
  ref_expr(var* p)
    : expr(ek_ref), ref(p)
  { }

  var* ref; // The referenced parameter.
};

/// A lambda abstraction.
struct abs_expr : expr
{
  abs_expr(var* v, expr* e)
    : expr(ek_abs), parm(v), body(e)
  { }

  var* parm; // The lambda parameter
  expr* body; // The abstracted expression
};

/// An application of arguments to a lambda abstraction.
struct app_expr : expr
{
  app_expr(expr* e1, expr* e2)
    : expr(ek_app), abs(e1), arg(e2)
  { }

  expr* abs;
  expr* arg;
};

/// A boolean literal.
struct bool_expr : expr
{
  bool_expr(bool b)
    : expr(ek_bool), val(b)
  { }

  bool val;
};

/// A conditional (if) expression.
struct cond_expr : expr
{
  cond_expr(expr* e1, expr* e2, expr* e3)
    : expr(ek_cond), cond(e1), tval(e2), fval(e3)
  { }
  
  expr* cond;
  expr* tval;
  expr* fval;
};

/// A placeholder for a value. This can be used only within the argument
/// list of a bind expression.
struct ph_expr : expr
{
  ph_expr()
    : expr(ek_ph)
  { }
};

/// A multi-parameter lambda abstraction of the form `\(x1, x2, ..., xn).e`.
/// This is syntactic sugar for '\x1.\x2. ... \xn.e.'
struct lambda_expr : expr
{
  lambda_expr(const var_seq& vs, expr* e)
    : expr(ek_lambda), parms(vs), body(e)
  { }

  lambda_expr(var_seq&& vs, expr* e)
    : expr(ek_lambda), parms(std::move(vs)), body(e)
  { }

  var_seq parms;
  expr* body;
};

/// A multi-argument application of the form `e(e1, e2, ..., en)`. This is
/// syntactic sugar for `e e1 e2 ... en`.
struct call_expr : expr
{
  call_expr(expr* e, const expr_seq& es)
    : expr(ek_call), abs(e), args(es)
  { }

  call_expr(expr* e, expr_seq&& es)
    : expr(ek_call), abs(e), args(std::move(es))
  { }

  expr* abs;
  expr_seq args;
};

/// A multi-argument partial application of the form `e(e1, e2, ..., en)` 
/// where zero or more ei are placeholders. In other words, the expression 
/// yields a function where non-placehoder values are bound to their 
/// parameters. If no placeholders are present, the resulting function is
/// a constant function (or generator if we had side effects).
struct bind_expr : expr
{
  bind_expr(expr* e, const expr_seq& es)
    : expr(ek_bind), abs(e), args(es)
  { }

  bind_expr(expr* e, expr_seq&& es)
    : expr(ek_bind), abs(e), args(std::move(es))
  { }

  expr* abs;
  expr_seq args;
};

/// A let expression of the form 'let x = e1 in e2'. This is syntactic sugar
/// for '(\x.e2) e1'.
struct let_expr : expr
{
  let_expr(var* x, expr* e1, expr* e2)
    : expr(ek_let), parm(x), init(e1), body(e2)
  { }

  var* parm;
  expr* init;
  expr* body;
};

/// A sequence expression of the form 'e1 ; e2'. This is syntactic sugar for
/// '(\_.e2) e1'. The parameter name is essentially ignored.
struct seq_expr : expr
{
  seq_expr(expr* e1, expr* e2)
    : expr(ek_seq), first(e1), second(e2)
  { }

  expr* first;
  expr* second;
};

/// Returns true if e is a value (i.e., in normal form).
bool is_value(const expr* e);

/// Returns true if is a lambda abstraction (of any kind).
bool is_lambda(const expr* e);

/// Returns true when e1 and e2 have the same syntax.
bool equal(const expr* e1, const expr* e2);

/// Returns true when e1 and e2 are alpha equivalent.
bool alpha_equivalent(const expr* e1, const expr* e2);

/// Returns true when e1 and e2 are beta equivalent.
bool beta_equivalent(const expr* e1, const expr* e2);

// -------------------------------------------------------------------------- //
// Printing

/// Print the expression e to os.
void print(std::ostream& os, const expr* e);

/// Print the expression e to std::cout.
void print(const expr* e);

/// Write the expression to the output stream.
std::ostream& operator<<(std::ostream& os, const expr* e);

// -------------------------------------------------------------------------- //
// Scope

/// A scope contains the list of variables that are visible within a region 
/// of code. This is more general than needed for untyped lambda calculus
/// in that it allows multiple variables at any particular scope.
struct scope : std::unordered_map<std::string, var*>
{
  // Declares a new variable at level l.
  var* declare(const std::string& s);

  // Returns the corresponding variable for s.
  var* lookup(const std::string& s);
};

/// The environment maintains information about a translation for the purpose
/// of name binding and lookup.
struct environment
{
  // Enter a new scope.
  void enter_scope() { stack.emplace_back(); }

  // Leave the current scope.
  void leave_scope() { stack.pop_back(); }

  // Declares a new variable with the name s.
  var* declare(const std::string& s);

  // Returns the innermost declaration of s.
  var* lookup(const std::string& s);
  
  std::vector<scope> stack; // The scope stack.
};

// A helper class that maintains scope.
struct scope_guard
{
  scope_guard(environment& cxt) : cxt(cxt) { cxt.enter_scope(); }
  ~scope_guard() { cxt.leave_scope(); }
  
  environment& cxt;
};

// -------------------------------------------------------------------------- //
// Desugaring

/// Returns the desugared form of an expression.
expr* desugar(expr* e);

// -------------------------------------------------------------------------- //
// Substitution

/// A substitution record is a set of substitutions. Each substitution
/// replaces a bound variable with an expression.
struct subst : std::unordered_map<var*, expr*>
{
  using std::unordered_map<var*, expr*>::unordered_map;
};

/// Substitute s through the expression e.
expr* substitute(expr* e, const subst& sub);

/// Substitute s through the expression e, given the environment env.
expr* substitute(expr* e, const subst& sub, environment& env);

// -------------------------------------------------------------------------- //
// Currying

/// Transform a lambda expression into a sequence of abstractions.
expr* curry(expr* e);

/// Transform a sequence of abstractions into a lambda expression.
expr* uncurry(expr* e);

// -------------------------------------------------------------------------- //
// Reduction (rewriting)

/// Perform a single-step reduction on e.
expr* reduce(expr* e);

// -------------------------------------------------------------------------- //
// Evaluation

enum value_kind
{
  vk_int,
  vk_abs,
};

using int_val = int;
using abs_val = const abs_expr*;

union value_rep
{
  int_val num;
  abs_val abs;
};

/// A value computed by an expression.
///
/// \todo This changes as we add language features.
struct value
{
  value(bool b) 
    : kind(vk_int)
  {
    rep.num = b; 
  }
  
  value(const abs_expr* e)
    : kind(vk_abs)
  {
    rep.abs = e;
  }

  value_kind kind;
  value_rep rep;
};

/// FIXME: Is this sufficient? Or do we need a stack.
struct store : std::unordered_map<var*, value>
{
  using std::unordered_map<var*, value>::unordered_map;
};

// Evaluate the expression e.
value evaluate(const expr* e);
