
#include "ast.hpp"

// FIXME: Is this right?
bool
equal(const var* a, const var* b)
{
  return a->name == b->name;
}

bool
equal(const var_seq& a, const var_seq& b)
{
  return std::equal(a.begin(), a.end(), b.begin(), [](const var* x, const var* y) {
    return equal(x, y);
  });
}

bool
equal(const expr_seq& a, const expr_seq& b)
{
  return std::equal(a.begin(), a.end(), b.begin(), [](const expr* x, const expr* y) {
    return equal(x, y);
  });
}

bool
equal_ref(const ref_expr* e1, const ref_expr* e2)
{
  return equal(e1->ref, e2->ref);
}

bool
equal_abs(const abs_expr* e1, const abs_expr* e2)
{
  return equal(e1->parm, e2->parm) && equal(e1->body, e2->body);
}

bool
equal_app(const app_expr* e1, const app_expr* e2)
{
  return equal(e1->abs, e2->abs) && equal(e1->arg, e2->arg);
}

bool
equal_bool(const bool_expr* e1, const bool_expr* e2)
{
  return e1->val == e2->val;
}

bool
equal_cond(const cond_expr* e1, const cond_expr* e2)
{
  return equal(e1->cond, e2->cond) &&
         equal(e1->tval, e2->tval) &&
         equal(e1->fval, e2->fval);
}

bool
equal_ph(const ph_expr* e1, const ph_expr* e2)
{
  return true;
}

bool
equal_lambda(const lambda_expr* e1, const lambda_expr* e2)
{
  return equal(e1->parms, e2->parms) && 
         equal(e1->body, e2->body);
}

bool
equal_call(const call_expr* e1, const call_expr* e2)
{
  return equal(e1->abs, e2->abs) &&
         equal(e1->args, e2->args);
}

bool
equal_bind(const bind_expr* e1, const bind_expr* e2)
{
  return equal(e1->abs, e2->abs) &&
         equal(e1->args, e2->args);
}

bool
equal_let(const let_expr* e1, const let_expr* e2)
{
  return equal(e1->parm, e2->parm) &&
         equal(e1->init, e2->init) &&
         equal(e1->body, e2->body);
}

bool
equal_seq(const seq_expr* e1, const seq_expr* e2)
{
  return equal(e1->first, e2->first) && 
         equal(e1->second, e2->second);
}

bool
equal(const expr* e1, const expr* e2)
{
  if (e1->kind != e2->kind)
    return false;
  switch (e1->kind) {
    case ek_ref:
      return equal_ref(static_cast<const ref_expr*>(e1), static_cast<const ref_expr*>(e2));
    case ek_abs:
      return equal_abs(static_cast<const abs_expr*>(e1), static_cast<const abs_expr*>(e2));
    case ek_app:
      return equal_app(static_cast<const app_expr*>(e1), static_cast<const app_expr*>(e2));
    case ek_bool:
      return equal_bool(static_cast<const bool_expr*>(e1), static_cast<const bool_expr*>(e2));
    case ek_cond:
      return equal_cond(static_cast<const cond_expr*>(e1), static_cast<const cond_expr*>(e2));
    case ek_ph:
      return equal_ph(static_cast<const ph_expr*>(e1), static_cast<const ph_expr*>(e2));
    case ek_lambda:
      return equal_lambda(static_cast<const lambda_expr*>(e1), static_cast<const lambda_expr*>(e2));
    case ek_call:
      return equal_call(static_cast<const call_expr*>(e1), static_cast<const call_expr*>(e2));
    case ek_bind:
      return equal_bind(static_cast<const bind_expr*>(e1), static_cast<const bind_expr*>(e2));
    case ek_let:
      return equal_let(static_cast<const let_expr*>(e1), static_cast<const let_expr*>(e2));
    case ek_seq:
      return equal_seq(static_cast<const seq_expr*>(e1), static_cast<const seq_expr*>(e2));
  }
}

// -------------------------------------------------------------------------- //
// Alpha equivalence

// FIXME: This isn't correct. We need to de-register equivalences when
// leaving binding contexts.

struct equivalence : std::unordered_map<const var*, const var*>
{
  const var* get(const var* v) const
  {
    auto iter = find(v);
    if (iter != end())
      return iter->second;
    else
      return nullptr;
  }

  bool contains(const var* v1, const var* v2) const
  {
    return get(v1) == v2 || get(v2) == v1;
  }

  void equate(const var* v1, const var* v2)
  {
    emplace(v1, v2);
    emplace(v2, v1);
  }
};

bool alpha_equivalent(const expr* e1, const expr* e2, equivalence& eq);

bool 
alpha_equivalent(const expr_seq& a, const expr_seq& b, equivalence& eq)
{
  return std::equal(a.begin(), a.end(), b.begin(), [&eq](const expr* x, const expr* y) {
    return alpha_equivalent(x, y, eq);
  });
}

bool
alpha_ref_eq(const ref_expr* e1, const ref_expr* e2, equivalence& eq)
{
  return eq.contains(e1->ref, e2->ref);
}

bool
alpha_abs_eq(const abs_expr* e1, const abs_expr* e2, equivalence& eq)
{
  eq.equate(e1->parm, e2->parm);
  return alpha_equivalent(e1->body, e2->body, eq);
}

bool
alpha_app_eq(const app_expr* e1, const app_expr* e2, equivalence& eq)
{
  return alpha_equivalent(e1->abs, e2->abs) &&
         alpha_equivalent(e1->arg, e2->arg);
}

bool
alpha_bool_eq(const bool_expr* e1, const bool_expr* e2, equivalence& eq)
{
  return e1->val == e2->val;
}

bool
alpha_cond_eq(const cond_expr* e1, const cond_expr* e2, equivalence& eq)
{
  return alpha_equivalent(e1->cond, e2->cond) &&
         alpha_equivalent(e1->tval, e2->tval) &&
         alpha_equivalent(e1->fval, e2->fval);
}

bool
alpha_ph_eq(const ph_expr* e1, const ph_expr* e2, equivalence& eq)
{
  return true;
}

bool
alpha_lambda_eq(const lambda_expr* e1, const lambda_expr* e2, equivalence& eq)
{
  if (e1->parms.size() != e2->parms.size())
    return false;
  auto iter1 = e1->parms.begin();
  auto iter2 = e2->parms.begin();
  while (iter1 != e1->parms.end()) {
    eq.equate(*iter1, *iter2);
    ++iter1;
    ++iter2;
  }
  return alpha_equivalent(e1->body, e2->body);
}

bool
alpha_call_eq(const call_expr* e1, const call_expr* e2, equivalence& eq)
{
  return alpha_equivalent(e1->abs, e2->abs, eq) &&
         alpha_equivalent(e1->args, e2->args, eq);
}

bool
alpha_bind_eq(const bind_expr* e1, const bind_expr* e2, equivalence& eq)
{
  return alpha_equivalent(e1->abs, e2->abs, eq) &&
         alpha_equivalent(e1->args, e2->args, eq);
}

bool
alpha_let_eq(const let_expr* e1, const let_expr* e2, equivalence& eq)
{
  if (!alpha_equivalent(e1->init, e2->init))
    return false;
  eq.equate(e1->parm, e2->parm);
  return alpha_equivalent(e1->body, e2->body);
}

bool
alpha_seq_eq(const seq_expr* e1, const seq_expr* e2, equivalence& eq)
{
  return alpha_equivalent(e1->first, e2->first) &&
         alpha_equivalent(e1->second, e2->second);
}

bool
alpha_equivalent(const expr* e1, const expr* e2, equivalence& eq)
{
  if (e1->kind != e2->kind)
    return false;
  switch (e1->kind) {
    case ek_ref:
      return alpha_ref_eq(static_cast<const ref_expr*>(e1), static_cast<const ref_expr*>(e2), eq);
    case ek_abs:
      return alpha_abs_eq(static_cast<const abs_expr*>(e1), static_cast<const abs_expr*>(e2), eq);
    case ek_app:
      return alpha_app_eq(static_cast<const app_expr*>(e1), static_cast<const app_expr*>(e2), eq);
    case ek_bool:
      return alpha_bool_eq(static_cast<const bool_expr*>(e1), static_cast<const bool_expr*>(e2), eq);
    case ek_cond:
      return alpha_cond_eq(static_cast<const cond_expr*>(e1), static_cast<const cond_expr*>(e2), eq);
    case ek_ph:
      return alpha_ph_eq(static_cast<const ph_expr*>(e1), static_cast<const ph_expr*>(e2), eq);
    case ek_lambda:
      return alpha_lambda_eq(static_cast<const lambda_expr*>(e1), static_cast<const lambda_expr*>(e2), eq);
    case ek_call:
      return alpha_call_eq(static_cast<const call_expr*>(e1), static_cast<const call_expr*>(e2), eq);
    case ek_bind:
      return alpha_bind_eq(static_cast<const bind_expr*>(e1), static_cast<const bind_expr*>(e2), eq);
    case ek_let:
      return alpha_let_eq(static_cast<const let_expr*>(e1), static_cast<const let_expr*>(e2), eq);
    case ek_seq:
      return alpha_seq_eq(static_cast<const seq_expr*>(e1), static_cast<const seq_expr*>(e2), eq);
  }
}

bool
alpha_equivalent(const expr* e1, const expr* e2)
{
  equivalence eq;
  return alpha_equivalent(e1, e2, eq);
}

// -------------------------------------------------------------------------- //
// Beta equivalence

/// FIXME: Evaluating two expressions is the easiest way to compute this, but
/// maybe not the best.
bool 
beta_equivalent(const expr* e1, const expr* e2)
{
  value v1 = evaluate(e1);
  value v2 = evaluate(e2);
  if (v1.kind != v2.kind)
    return false;
  switch (v1.kind) {
    case vk_int:
      return v1.rep.num == v2.rep.num;
    case vk_abs:
      return v1.rep.abs == v2.rep.abs;
  }
}

