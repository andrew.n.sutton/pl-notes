
#include "ast.hpp"

value evaluate(const expr* e, store& s);

/// The value of a reference is the one to which it is bound.
value
evaluate_ref(const ref_expr* e, store& s)
{
  auto iter = s.find(e->ref);
  if (iter != s.end())
    return iter->second;
  else
    throw std::runtime_error("invalid reference");
}

/// Add a binding to the store and evaluate the body of the
/// abstraction.
value
evalaute_app(const app_expr* e, store& s)
{
  value abs = evaluate(e->abs, s);
  value arg = evaluate(e->arg, s);

  // FIXME: Assert that abs is, in fact, an abstraction.


  // Bind the parameter to its argument.
  const abs_expr* fn = abs.rep.abs;
  s.insert({fn->parm, arg});

  // Evaluate the body.
  return evaluate(fn->body, s);
}

/// An abstraction is a value.
value
evalaute_abs(const abs_expr* e, store& s)
{
  return value{e};
}

value
evalaute_bool(const bool_expr* e, store& s)
{
  return value{e->val};
}

value
evalaute_cond(const cond_expr* e, store& s)
{
  value cond = evaluate(e->cond, s);
  if (cond.kind != vk_int)
    throw std::runtime_error("invalid condition");
  if (cond.rep.num)
    return evaluate(e->tval, s);
  else
    return evaluate(e->fval, s);
}

value 
evaluate(const expr* e, store& s)
{
  switch (e->kind) {
    case ek_ref:
      return evaluate_ref(static_cast<const ref_expr*>(e), s);
    case ek_app:
      return evalaute_app(static_cast<const app_expr*>(e), s);
    case ek_abs:
      return evalaute_abs(static_cast<const abs_expr*>(e), s);
    case ek_bool:
      return evalaute_bool(static_cast<const bool_expr*>(e), s);
    case ek_cond:
      return evalaute_cond(static_cast<const cond_expr*>(e), s);
    case ek_ph:
    case ek_lambda:
    case ek_call:
    case ek_bind:
    case ek_let:
    case ek_seq:
      throw std::logic_error("evaluation of sugared expression");
  }
}

value
evaluate(const expr* e)
{
  store s;
  return evaluate(e, s);
}
