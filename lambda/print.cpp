#include "ast.hpp"

#include <iostream>

// FIXME: Define precedence so we can add parens intelligently.

void
print_var(std::ostream& os, const var* v)
{
  // The "unique name" of a variable is determined by its address.
  // os << v->name << '@' << (void*)v;
 
  os << v->name;
}

void
print(std::ostream& os, const var* v)
{
  return print_var(os, v);
}

void
print_ref(std::ostream& os, const ref_expr* e)
{
  print(os, e->ref);
}

void
print_abs(std::ostream& os, const abs_expr* e)
{
  os << '\\';
  print(os, e->parm);
  os << '.';
  print(os, e->body);
}

void
print_app(std::ostream& os, const app_expr* e)
{
  os << '(';
  
  // Further enclose the term if needed.
  if (is_lambda(e->abs))
    os << '(';
  print(os, e->abs);
  if (is_lambda(e->abs))
    os << ')';
  
  os << ' ';
  print(os, e->arg);
  os << ')';
}

void
print_bool(std::ostream& os, const bool_expr* e)
{
  if (e->val)
    os << "true";
  else
    os << "false";
}

void
print_cond(std::ostream& os, const cond_expr* e)
{
  os << "if" << ' ';
  print(os, e->cond);
  os << ' ' << "then" << ' ';
  print(os, e->tval);
  os << ' ' << "else" << ' ';
  print(os, e->fval);
}

void
print_ph(std::ostream& os, const ph_expr* e)
{
  os << '_';
}

void
print_lambda(std::ostream& os, const lambda_expr* e)
{
  os << '\\' << '(';
  for (auto iter = e->parms.begin(); iter != e->parms.end(); ++iter) {
    print(os, *iter);
    if (std::next(iter) != e->parms.end())
      os << ',' << ' ';
  }
  os << ')' << '.';
  print(os, e->body);
}

void
print_exprs(std::ostream& os, const expr_seq& es)
{
  for (auto iter = es.begin(); iter != es.end(); ++iter) {
    print(os, *iter);
    if (std::next(iter) != es.end())
      os << ',' << ' ';
  }
}

void
print_call(std::ostream& os, const call_expr* e)
{
  os << '(';
  print(os, e->abs);
  os << ')';
  os << '(';
  print_exprs(os, e->args);
  os << ')';
}

void
print_bind(std::ostream& os, const bind_expr* e)
{
  os << '(';
  print(os, e->abs);
  os << ')';
  os << "(:";
  print_exprs(os, e->args);
  os << ":)";
}

void
print_let(std::ostream& os, const let_expr* e)
{
  os << "let" << ' ';
  print(os, e->parm);
  os << ' ' << '=' << ' ';
  print(os, e->init);
  os << ' ' << "in" << ' ';
  print(os, e->body);
}

void
print_seq(std::ostream& os, const seq_expr* e)
{
  print(os, e->first);
  os << ' ' << ';' << ' ';
  print(os, e->second);
}

void
print(std::ostream& os, const expr* e)
{
  switch (e->kind) {
    case ek_ref:
      return print_ref(os, static_cast<const ref_expr*>(e));
    case ek_abs:
      return print_abs(os, static_cast<const abs_expr*>(e));
    case ek_app:
      return print_app(os, static_cast<const app_expr*>(e));
    case ek_bool:
      return print_bool(os, static_cast<const bool_expr*>(e));
    case ek_cond:
      return print_cond(os, static_cast<const cond_expr*>(e));
    case ek_ph:
      return print_ph(os, static_cast<const ph_expr*>(e));
    case ek_lambda:
      return print_lambda(os, static_cast<const lambda_expr*>(e));
    case ek_call:
      return print_call(os, static_cast<const call_expr*>(e));
    case ek_bind:
      return print_bind(os, static_cast<const bind_expr*>(e));
    case ek_let:
      return print_let(os, static_cast<const let_expr*>(e));
    case ek_seq:
      return print_seq(os, static_cast<const seq_expr*>(e));
  }
}

void
print(const expr* e)
{
  print(std::cout, e);
  std::cout << '\n';
}

std::ostream& 
operator<<(std::ostream& os, const expr* e)
{
  print(os, e);
  return os;
}
