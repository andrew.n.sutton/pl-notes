
#include "ast.hpp"

// Generate a unique 'name' for v by registering a new declaration.
var*
rename(var* v, environment& env)
{
  return env.declare(v->name);
}

// Find the replacement name for v. Throws an exception if the variable 
// is free.
var*
lookup(var* v, environment& env)
{
  var* r = env.lookup(v->name);
  if (!r)
    throw std::runtime_error("free variable");
  return r;
}

expr* substitute(expr* e, const subst& s, environment& env);

/// Replace the identifier with its substitution or a reference to
/// it's bound variable.
///
///     [x->s]x = s
///     [x->s]y = lookup(y)   if y != x
///
/// We need to perform a lookup in order to ensure that all variables have
/// unique names.
expr*
substitute_ref(ref_expr* e, const subst& s, environment& env)
{
  auto iter = s.find(e->ref);
  if (iter != s.end())
    // Replace the reference with its substitution.
    return iter->second; 
  else  
    // Replace the reference with its renamed version.
    return new ref_expr(lookup(e->ref, env));
}

/// [x->s]\y.e = \rename(y).[x->s]e
///
/// We always rename y, which ensures that the substitution avoids capture.
/// In this context, renaming means creating a new variable and then replacing
/// references to it.
expr*
substitute_abs(abs_expr* e, const subst& s, environment& env)
{
  scope_guard g(env);
  var* parm = rename(e->parm, env);
  expr* body = substitute(e->body, s, env);
  return new abs_expr(parm, body);
}

/// [x->s](e1 e2) = [x->s]e1 [x->s]e2
///
/// Apply substitution to both operands.
expr*
substitute_app(app_expr* e, const subst& s, environment& env)
{
  expr* abs = substitute(e->abs, s, env);
  expr* arg = substitute(e->arg, s, env);
  return new app_expr(abs, arg);
}

/// [x->s]true = true
/// [x->s]false = false
expr*
substitute_bool(bool_expr* e, const subst& s, environment& env)
{
  return e;
}

/// [x->s]if e1 then e2 else e3 = if [x->s]e1 then [x->s]e2 else [x->s]e2
expr*
substitute_cond(cond_expr* e, const subst& s, environment& env)
{
  expr* cond = substitute(e->cond, s, env);
  expr* tval = substitute(e->tval, s, env);
  expr* fval = substitute(e->cond, s, env);
  return new cond_expr(cond, tval, fval);
}

/// [x->s]_ = _
expr*
substitute_ph(ph_expr* e, const subst& s, environment& env)
{
  return e;
}

/// [x->s]\(x1, x2, ..., xn).e = \(x1, x2, ..., xn).[x->s]e
expr*
substitute_lambda(lambda_expr* e, const subst& s, environment& env)
{
  scope_guard g(env);
  var_seq parms;
  for (var* x : e->parms)
    parms.push_back(rename(x, env));
  expr* body = substitute(e->body, s, env);
  return new lambda_expr(std::move(parms), body);

}

/// [x->s]e(e1, e2, ..., en) = [x->s]e([x->s]e1, [x->s]e2, ..., [x->s]en)
expr*
substitute_call(call_expr* e, const subst& s, environment& env)
{
  expr* abs = substitute(e->abs, s, env);
  expr_seq args;
  for (expr* a : e->args)
    args.push_back(substitute(a, s, env));
  return new call_expr(abs, std::move(args));
}

/// [x->s]e(:e1, e2, ..., en:) = [x->s]e(:[x->s]e1, [x->s]e2, ..., [x->s]en:)
expr*
substitute_bind(bind_expr* e, const subst& s, environment& env)
{
  expr* abs = substitute(e->abs, s, env);
  expr_seq args;
  for (expr* a : e->args)
    args.push_back(substitute(a, s, env));
  return new bind_expr(abs, std::move(args));
}

/// [x->s](let x = e1 in e2) = (let x = [x->s]e1 in [x->s]e2)
///
/// The variable x is renamed in the body of the function.
expr*
substitute_let(let_expr* e, const subst& s, environment& env)
{
  expr* init = substitute(e->init, s, env);
  scope_guard g(env);
  var* x = rename(e->parm, env);
  expr* body = substitute(e, s, env);
  return new let_expr(x, init, body);  
}

/// [x->s](e1; e2) = [x->s]e1; [x->s]e2
expr*
substitute_seq(seq_expr* e, const subst& s, environment& env)
{
  expr* first = substitute(e->first, s, env);
  expr* second = substitute(e->second, s, env);
  return new seq_expr(first, second);
}

/// Substitute through the expression using the given environment.
expr*
substitute(expr* e, const subst& s, environment& env)
{
  switch (e->kind) {
    case ek_ref:
      return substitute_ref(static_cast<ref_expr*>(e), s, env);
    case ek_abs:
      return substitute_abs(static_cast<abs_expr*>(e), s, env);
    case ek_app:
      return substitute_app(static_cast<app_expr*>(e), s, env);
    case ek_bool:
      return substitute_bool(static_cast<bool_expr*>(e), s, env);
    case ek_cond:
      return substitute_cond(static_cast<cond_expr*>(e), s, env);
    case ek_ph:
      return substitute_ph(static_cast<ph_expr*>(e), s, env);
    case ek_lambda:
      return substitute_lambda(static_cast<lambda_expr*>(e), s, env);
    case ek_call:
      return substitute_call(static_cast<call_expr*>(e), s, env);
    case ek_bind:
      return substitute_bind(static_cast<bind_expr*>(e), s, env);
    case ek_let:
      return substitute_let(static_cast<let_expr*>(e), s, env);
    case ek_seq:
      return substitute_seq(static_cast<seq_expr*>(e), s, env);
  }
}

/// Substitute through the expression.
expr*
substitute(expr* e, const subst& s)
{
  environment env;
  return substitute(e, s, env);
}
